import  Knex from 'knex';

export class BoardService{
    constructor(private knex:Knex){

    }

    allBoards(){
        return this.knex.select('id').from('boards').orderBy('created_at',"desc");
    }


    async getBoard(boardId:number){
        return (await (this.knex.select("*").from('boards').where("id",boardId).limit(1)))[0];
    }

    async createBoard(){
        return (await this.knex.insert({ squares: JSON.stringify(Array(9).fill(null))})
                    .into('boards').returning("id"))[0];
    }

    nextStep(boardId:number,player:string,index:number){
        return this.knex.transaction(async (trx)=>{
            try{
                const results = await trx.select("*").from('boards').where("id",boardId);
                const board = results[0];
                const squares = board.squares as Array<string|null>;
                squares[index] = player;
                await trx('boards').update({squares:JSON.stringify(squares)})
                                .where('id',boardId);
            }catch(e){
                console.log(e);
            }
        });
    }

    async reset(boardId:number){
        return await this.knex('boards').where('id',boardId).del();
    }

}

