

export interface Board{
    id?:number
    squares: Array<string|null>
}


export interface Score{
    winner:string,
    name:string
    squares: Array<string|null>
}

