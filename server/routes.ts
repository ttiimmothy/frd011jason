import express from 'express';
import { boardController, scoreController } from './main';

export const routes = express.Router();
// Board routes
routes.get('/boards',boardController.list);
routes.get('/boards/:id',boardController.get);
routes.post('/boards',boardController.post);
routes.put('/boards/:id',boardController.put);
routes.delete('/boards/:id',boardController.delete);

// Student routes
routes.get('/scores',scoreController.get);
routes.post('/scores',scoreController.post);