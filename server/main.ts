import express from 'express';
import Knex from 'knex';
import cors from 'cors';
import { BoardService } from './services/BoardService';
import { ScoreService } from './services/ScoreService';
import { ScoreController } from './controllers/ScoreController';
import { BoardController } from './controllers/BoardController';
const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])

const app = express();

app.use(cors());
app.use(express.urlencoded({extended:true}));
app.use(express.json());

const boardService = new BoardService(knex);
const scoreService = new ScoreService(knex);

export const boardController = new BoardController(boardService);
export const scoreController = new ScoreController(scoreService);

import {routes} from './routes';

app.use('/',routes);

const PORT = 8050;
app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}`);
	// console.log(process.env.DB_NAME);
    if(process.env.CI){
        process.exit(0);
    }
});

