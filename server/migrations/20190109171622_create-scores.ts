import  Knex from "knex";

exports.up = async function (knex: Knex) {
    const hasTable = await knex.schema.hasTable('scores');
    if(!hasTable){
        return await knex.schema.createTable('scores',(table)=>{
            table.increments();
            table.string('winner');
            table.string('name');
            table.jsonb("squares")
            table.timestamps(false,true);
        });
    }else{
        return Promise.resolve();
    }
};

exports.down = function (knex: Knex) {
    return knex.schema.dropTable('scores');
};
