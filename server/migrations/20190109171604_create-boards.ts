import  Knex from "knex";

exports.up = async function (knex: Knex) {
    const hasTable = await knex.schema.hasTable('boards');
    if(!hasTable){
        return await knex.schema.createTable('boards',(table)=>{
            table.increments();
            table.jsonb("squares")
            table.timestamps(false,true);
        })
    }else{
        return Promise.resolve();
    }
};

exports.down = function (knex: Knex) {
    return knex.schema.dropTable('boards');
};
