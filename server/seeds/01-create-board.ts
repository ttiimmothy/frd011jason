import * as Knex from "knex";

exports.seed = function (knex: Knex): Promise<any> {
    // Deletes ALL existing entries
    return knex("boards").truncate()
        .then(function () {
            // Inserts seed entries
            return knex("boards").insert([{squares: JSON.stringify(Array(9).fill(null))},
			{squares: JSON.stringify(Array(9).fill(null))},
			{squares: JSON.stringify(Array(9).fill(null))}]);
        });
};
