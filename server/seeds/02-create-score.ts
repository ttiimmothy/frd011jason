import * as Knex from "knex";

exports.seed = function (knex: Knex): Promise<any> {
    // Deletes ALL existing entries
    return knex("scores").truncate()
        .then(function () {
            // Inserts seed entries
            return knex("scores").insert([{
				winner: "X",
				name: "ttiimmothhy",
				squares: JSON.stringify([null,null,"X",null,"X",null,"X",null,null])
			},{
				winner: "O",
				name: "kkken",
				squares: JSON.stringify(["O","X",null,"O","X",null,"O",null,null])
			}]);
        });
};
