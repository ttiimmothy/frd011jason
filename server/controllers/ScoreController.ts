import {Request,Response} from 'express';
import { ScoreService } from '../services/ScoreService';
import { Score } from '../services/models';

export class ScoreController{

    constructor(private scoreService:ScoreService){}

    get = async (req:Request,res:Response)=>{
        try{
            const scores = await this.scoreService.allScores()
            const result = scores.map((score:Score)=>({
                winner:score.winner,
                name: score.name,
                squares: score.squares
            }))
			// console.log(result)
            res.json({isSuccess:true,data:result});
        }catch(e){
            res.json({isSuccess:false,msg:e.toString()});
        }
    }

    post = async(req:Request,res:Response)=>{
        try{
            const {winner,name,squares}= req.body
            await this.scoreService.addScore(
                winner,
                name,
                JSON.stringify(squares));
            res.json({isSuccess:true});
        }catch(e){
            res.json({isSuccess:false,msg:e.toString()});
        }
    }
}