import {Request,Response} from 'express';
import { BoardService } from '../services/BoardService';
import { Board } from '../services/models';

export class BoardController{

    constructor(private boardService:BoardService){}

    list = async (req:Request,res:Response)=>{
        try{
            const results:Board[] = await this.boardService.allBoards();
            res.json({isSuccess:true,data:results.map((row:Board)=>row.id)});
        }catch(e){
            res.json({isSuccess:false,msg:e.toString()});
        }
    }

    get = async (req:Request,res:Response)=>{
        try{
            const id = parseInt(req.params.id);
            if(isNaN(id)){
                res.status(400).json({msg:"Id is not a number"})
                return;
            }
            const result:Board = await this.boardService.getBoard(id);    
            res.json({isSuccess:true,data:result});
        }catch(e){
            res.json({isSuccess:false,msg:e.toString()});
        }
    }

    post = async (req:Request,res:Response)=>{
        try{
            const id = await this.boardService.createBoard();    
            res.json({isSuccess:true,data:{id}});
        }catch(e){
            res.json({isSuccess:false,msg:e.toString()});
        }
    }

    put = async(req:Request,res:Response)=>{
        try{
            const id = parseInt(req.params.id);
            if(isNaN(id)){
                res.status(400).json({msg:"Id is not a number"})
                return;
            }
            const {player,index} = req.body;
            await this.boardService
                            .nextStep(id,player,index);
            res.json({isSuccess:true});
        }catch(e){
            res.json({isSuccess:false,msg:e.toString()});
        }
        
    }

 
    delete = async (req:Request,res:Response)=>{
        try{
            const id = parseInt(req.params.id);
            if(isNaN(id)){
                res.status(400).json({msg:"Id is not a number"})
                return;
            }
            await this.boardService.reset(id);
            res.json({isSuccess:true});
        }catch(e){
            res.json({isSuccess:false,msg:e.toString()});
        }
    }

}