import React from 'react';
import {useDispatch,useSelector} from 'react-redux';
import {IRootState} from './store';
import {ListGroup,ListGroupItem} from 'reactstrap';
import './Scores.css';
import {useEffect} from 'react';
// import { getScores} from './score/thunks';
import {getScoresSuccess,failed} from './score/actions';
import {IScore} from './score/state';

// Custom hook implementation
// Tight coupling of redux and react
function useFetch() {
	const dispatch = useDispatch();

	const {REACT_APP_API_SERVER} = process.env;

	useEffect(() => {
		fetch(`${REACT_APP_API_SERVER}/scores`).then(res => res.json()).then(res => {
			console.log(res)
			dispatch(getScoresSuccess(res.data as IScore[]));
		}).catch((e) => {
			failed("GET_SCORES_FAILED",e);
		});
	},[]);
}


export default function Score() {
	const scores = useSelector((state: IRootState) => state.score.scores);
	// console.log(scores);
	useFetch();
	return (
		<div>
			<h1 className="text-danger">
				ScoreBoard
			</h1>
			<ListGroup>
				{
					scores.map((score,i) => (
						<ListGroupItem key={i}>
							Name: {score.name}<br />
							Winner: {score.winner}<br />
							Board:
							<div className="board">
								{Array(9).fill(0).map(
									(num,i) => (
										<div className="square score-square" key={i}>
											{score.squares[i]}
										</div>
									))}
							</div>
						</ListGroupItem>
					))
				}
			</ListGroup>
		</div>
	);
}

