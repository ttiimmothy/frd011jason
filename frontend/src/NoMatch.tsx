import React from 'react';
import {Redirect} from 'react-router';
import {useState,useEffect} from 'react';
// import { useHistory } from 'react-router-dom'

export default function NoMatch() {
	const [redirectNow,setRedirectNow] = useState(false);

	// const history = useHistory()

	useEffect(() => {
		window.setTimeout(() => setRedirectNow(true),1500);
	},[]);

	// useEffect(() => {
	//     if(!redirectNow) return
	//     history.push("/")
	// }, [redirectNow])

	if(redirectNow) {
		return <Redirect to="/" />;
	}

	return (
		<div>
			<h1 className="text-primary">
				It seems that you enter a wrong page
			</h1>
			<h2 className="text-muted">
				Now going back to the main page
			</h2>
		</div>
	);
}
