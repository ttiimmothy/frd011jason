import  React from 'react';
import {match} from 'react-router-dom';


function About(props:{match:match}){
    return(
        <div>
            <h1 className="text-danger"> About Tecky Academy</h1>
            <p className="text-muted">
            Tecky Academy is founded with the belief of providing affordable and career-centric technology education to who are interested in pursuing a career in the technology field.  “Discover the tech Discover yourself” is our motto to help everyone to unleash their tech potential . We are formed by a  team of experienced Software Developers with years of planning, developing and administering software projects and systems.
            </p>
        </div>
    )
}

export default About;