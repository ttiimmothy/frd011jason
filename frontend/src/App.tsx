import React from "react";
import Board from './Board';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import store,{history} from './store';
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'connected-react-router';
import {NavLink,Switch,Route,Redirect} from 'react-router-dom';
import Home from './Home';
import Scores from './Scores';
import About from './About';
import NoMatch from './NoMatch';
import Helmet from "react-helmet";

function App() {
	return (
		<Provider store={store}>
			<ConnectedRouter history={history}>
				<Helmet>
					<title>Tic Tac Toe</title>
				</Helmet>
				<div>
					<nav className="nav-bar">
						<NavLink to="/home" className="link">Home</NavLink>
						<NavLink to="/scores" className="link">Score Board</NavLink>
						<NavLink to="/about" className="link">About us</NavLink>
					</nav>
					<div className="main">
						<Switch>
							<Route path="/home" exact component={Home} />
							<Route path="/scores" component={Scores} />
							<Route path="/board/:id" component={Board} />
							<Route path="/about" component={About} />
							<Route><Redirect to="/home" /></Route>
							<Route component={NoMatch} />
						</Switch>
					</div>
				</div>
			</ConnectedRouter>
		</Provider>
	);
}

export default App;
