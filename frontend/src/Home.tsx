import React from 'react';
import {Button,ListGroup,ListGroupItem} from 'reactstrap';
import {IRootState} from './store';
import {newBoard,allBoard} from './board/thunks';
import {useSelector,useDispatch} from 'react-redux';
import {Link} from 'react-router-dom';
import {useEffect} from 'react';


export default function Home() {
	const previousBoardIds = useSelector((state: IRootState) => state.board.previousBoardIds);
	// console.log(previousBoardIds)
	// const { REACT_APP_API_SERVER } = process.env

	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(allBoard());
	},[]);

	return (
		<div>
			<h1 className="text-danger"> My Tic-tac-toe Game</h1>
			<p className="text-muted">Welcome to my tic-tac-toe game with history
			</p>
			<Button outline color="danger" onClick={() => dispatch(newBoard())}>Start New Game</Button>
			<h3>--- or ---</h3>
			<div>
				<div className="text-muted">Continue from one of the following game</div>
				<ListGroup>
					{
						previousBoardIds &&
						previousBoardIds.map((previousBoardId,i) => (
							<Link key={i} to={`/board/${previousBoardId}`}>
								<ListGroupItem>
									Board {i + 1}<br />
								</ListGroupItem>
							</Link>
						))
					}
				</ListGroup>
			</div>
		</div>
	);
}


