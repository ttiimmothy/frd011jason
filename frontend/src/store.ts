import {createStore, combineReducers, compose, applyMiddleware} from "redux";
import {IBoardState} from "./board/state";
import {IBoardActions} from "./board/actions";
import {boardReducers} from "./board/reducers";
import logger from "redux-logger";
import {createBrowserHistory} from "history";
import {RouterState, connectRouter, routerMiddleware} from "connected-react-router";
import {IScoreState} from "./score/state";
import {IScoreActions} from "./score/actions";
import {scoreReducers} from "./score/reducers";
import thunk, {ThunkDispatch as OldThunkDispatch} from "redux-thunk";

declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
    }
}

export const history = createBrowserHistory();

export interface IRootState {
    board: IBoardState;
    score: IScoreState;
    router: RouterState;
}

type IRootAction = IBoardActions | IScoreActions;

const rootReducer = combineReducers<IRootState>({
    board: boardReducers,
    score: scoreReducers,
    router: connectRouter(history),
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export type ThunkDispatch = OldThunkDispatch<IRootState, null, IRootAction>;

export default createStore<IRootState, IRootAction, {}, {}>(rootReducer,
	composeEnhancers(
		applyMiddleware(thunk),
		applyMiddleware(routerMiddleware(history)),
		applyMiddleware(logger)
	)
);
