import {IScore} from './state';

export function getScoresSuccess(scores: IScore[]) {
	return {
		type: "GET_SCORES" as "GET_SCORES",
		scores
	};
}

export function addWinnerSuccess(name: string,
	squares: Array<string | null>,
	winner: string) {
	return {
		type: "ADD_WINNER" as "ADD_WINNER",
		name,
		squares,
		winner
	};
}

export function failed(type: FAILED,msg: string) {
	return {
		type,
		msg
	};
}

type FAILED = "ADD_WINNER_FAILED" | "GET_SCORES_FAILED";
type ScoreActionCreators = typeof addWinnerSuccess | typeof getScoresSuccess | typeof failed;

export type IScoreActions = ReturnType<ScoreActionCreators>;