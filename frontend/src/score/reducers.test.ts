import {scoreReducers} from './reducers';
import { IScoreState } from './state';
import {  addWinnerSuccess } from './actions';


describe('Score Reducer',()=>{
    let initialState:IScoreState;

    beforeEach(()=>{
        initialState = {
            scores:[]
        };
    })

    it("should add new winner",()=>{
        const finalState = scoreReducers( initialState, addWinnerSuccess("Tecky",['O','X','X','O',null,null,'O',null,null],'O'));
        expect(finalState).toEqual({
            scores:[
                {
                    name:"Tecky",
                    squares: ['O','X','X','O',null,null,'O',null,null],
                    winner:'O'
                }
            ]
        });
    });
});