import { IScoreState } from './state';
import { IScoreActions } from './actions';

const initialState = {
    scores:[]
}

export const scoreReducers = (state:IScoreState = initialState,action:IScoreActions):IScoreState=>{
    switch(action.type){
        case "ADD_WINNER":
            return {
				...state.scores,
                scores:state.scores.concat([{
					name: action.name,
					squares:action.squares,
					winner: action.winner
				}])
            }
        case "GET_SCORES":
            return {
				...state,
                scores: action.scores
            }
        default:
            return state
    }
}