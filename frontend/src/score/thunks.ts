import {Dispatch} from "redux";
import {IScoreActions, addWinnerSuccess, failed, getScoresSuccess} from "./actions";
import {IScore} from "./state";

const {REACT_APP_API_SERVER} = process.env;

export function addWinner(name: string, squares: Array<string | null>, winner: string) {
    return async (dispatch: Dispatch<IScoreActions>) => {
        const res = await fetch(`${REACT_APP_API_SERVER}/scores`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                winner,
                squares,
                name,
            }),
        });
        const result = await res.json();

        if (result.isSuccess) {
            dispatch(addWinnerSuccess(name, squares, winner));
        } else {
            dispatch(failed("ADD_WINNER_FAILED", result.msg));
        }
    };
}

export function getScores() {
    return async (dispatch: Dispatch<IScoreActions>) => {
        const res = await fetch(`${REACT_APP_API_SERVER}/scores`, {
            method: "GET",
        });
        const result = await res.json();

        if (result.isSuccess) {
            dispatch(getScoresSuccess(result.data as IScore[]));
        } else {
            dispatch(failed("GET_SCORES_FAILED", result.msg));
        }
    };
}
