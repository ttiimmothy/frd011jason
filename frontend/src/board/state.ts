


export interface IBoardState{
    squares : Array<string|null>
    oIsNext: boolean
    previousBoardIds: Array<number>
}
