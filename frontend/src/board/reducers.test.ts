import {boardReducers} from './reducers';
import { IBoardState } from './state';
import {  nextStepSuccess, resetSuccess } from './actions';


describe('Board Reducer',()=>{
    let initialState:IBoardState;

    beforeEach(()=>{
        initialState = {
            squares: [null,null,'O',null,null,null,null,null,null],
            oIsNext:false,
            previousBoardIds:[]
        };
    })

    it("should perform next step",()=>{
        const finalState = boardReducers( initialState, nextStepSuccess('X',4));
        expect(finalState).toEqual({
            squares: [null,null,'O',null,'X',null,null,null,null],
            oIsNext: true,
            previousBoardIds:[]
        });
    });
   
    it("should reset",()=>{
        const finalState = boardReducers( initialState, resetSuccess());
        expect(finalState).toEqual({
            squares: [],
            oIsNext: true,
            previousBoardIds:[]
        });
    });
});