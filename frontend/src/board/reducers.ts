import { IBoardState } from './state';
import { IBoardActions } from './actions';

const initialState = {
    squares: [],
    oIsNext: true,
    previousBoardIds:[]
}

export const boardReducers = (state:IBoardState = initialState,action:IBoardActions)=>{
    switch(action.type){
        case "ALL_BOARD":
            return {
                ...state,
                previousBoardIds: action.previousBoardIds
            }
        case "GET_BOARD":
            return {
                ...state,
                squares: action.squares,
                oIsNext: action.squares.filter((square)=>square).length % 2 == 0
            }
        case "NEXT_STEP":
            const {player,index} = action;
            const newSquares = state.squares.slice();
            newSquares[index] = player;
            return {
                ...state,
                squares: newSquares,
                oIsNext: !state.oIsNext
            }
        case "RESET":
            return {
                ...state,
                squares:[],
                oIsNext:true
            }
        default:
            return state
    }
}