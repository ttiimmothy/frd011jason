import {Dispatch} from 'redux';
import {IBoardActions, getBoardSuccess, failed, nextStepSuccess, resetSuccess, allBoardSuccess} from './actions';
import { push, CallHistoryMethodAction } from 'connected-react-router';

const {REACT_APP_API_SERVER} =  process.env;

export function newBoard(){
    return async (dispatch:Dispatch<IBoardActions|CallHistoryMethodAction>)=>{
        const res = await fetch(`${REACT_APP_API_SERVER}/boards`,{
            method:"POST"
        });
        const result = await res.json();

        if(result.isSuccess){
            dispatch(push(`/board/${result.data.id}`));
        }else{
            dispatch(failed("NEW_BOARD_FAILED",result.msg))
        }
    }
}


export function getBoard(boardId:number){
    return async (dispatch:Dispatch<IBoardActions>)=>{
        const res = await fetch(`${REACT_APP_API_SERVER}/boards/${boardId}`);
        const result = await res.json();

        if(result.isSuccess){
            dispatch(getBoardSuccess(result.data.squares as Array<string|null>));
        }else{
            dispatch(failed("GET_BOARD_FAILED",result.msg))
        }
    }
}

export function allBoard(){
    return async (dispatch:Dispatch<IBoardActions>)=>{
        const res = await fetch(`${REACT_APP_API_SERVER}/boards`);
        const result = await res.json();
		console.log(result);

        if(result.isSuccess){
            dispatch(allBoardSuccess(result.data as Array<number>));
        }else{
            dispatch(failed("ALL_BOARD_FAILED",result.msg))
        }
    }
}

export function nextStep(boardId:number,player:string,index:number){
    return async (dispatch:Dispatch<IBoardActions>)=>{
        const res = await fetch(`${REACT_APP_API_SERVER}/boards/${boardId}`,{
            method:"PUT",
            headers:{
                "Content-Type":"application/json"
            },
            body:JSON.stringify({
                player,index
            })
        });
        const result = await res.json();

        if(result.isSuccess){
            dispatch(nextStepSuccess(player,index));
        }else{
            dispatch(failed("NEXT_STEP_FAILED",result.msg))
        }
    }
}

export function reset(boardId:number){
    return async (dispatch:Dispatch<IBoardActions>)=>{
        const res = await fetch(`${REACT_APP_API_SERVER}/boards/${boardId}`,{
            method:"DELETE"
        });
        const result = await res.json();

        if(result.isSuccess){
            dispatch(resetSuccess());
        }else{
            dispatch(failed("RESET_FAILED",result.msg))
        }
    }
}