import configureMockStore, { MockStoreEnhanced } from 'redux-mock-store'
import thunk from 'redux-thunk'
import { getBoardSuccess, failed  } from './actions';
import {getBoard} from './thunks';
import fetchMock from 'fetch-mock';
import { IRootState, ThunkDispatch } from '../store';



describe('Board thunks', () => {
  let store: MockStoreEnhanced<IRootState,ThunkDispatch>;
  
  beforeEach(()=>{
    const mockStore = configureMockStore<IRootState,ThunkDispatch>([thunk])
    store = mockStore();
  })
  
  it('should get board successfully', async () => {
    const result = {
        isSuccess: true,
        data: {
            squares: Array(9).fill(null)
        }
    };
    fetchMock.get(`${process.env.REACT_APP_API_SERVER}/boards/1`, 
        {body:result,status:200});

    const expectedActions = [
      getBoardSuccess(result.data.squares),
    ]
    await store.dispatch(getBoard(1));
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should fail to get boards', async () => {
    const result = {
        isSuccess:false,
        msg: "Failed to get boards"
    };
    fetchMock.get(`${process.env.REACT_APP_API_SERVER}/boards/1`, 
        {body:result,status:200});

    const expectedActions = [
      failed('GET_BOARD_FAILED',result.msg)
    ]

    await store.dispatch(getBoard(1));
    expect(store.getActions()).toEqual(expectedActions);
  });
  

  afterEach(() => {
    fetchMock.restore()
  });
})