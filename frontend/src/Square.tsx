import  React from 'react';
import './Square.css';

export interface ISquareProps {
    value: string | null
    squareOnClick: ()=> void
}
  

function Square(props:ISquareProps){
  return (
    <button className="square" onClick={props.squareOnClick}>
      {props.value}
    </button>
  );
}


export default Square;