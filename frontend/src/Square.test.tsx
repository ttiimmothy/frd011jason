import '@testing-library/jest-dom'
import React from 'react';
import {render, screen, fireEvent} from '@testing-library/react'
import Square, {ISquareProps} from './Square';

describe("Square component",()=>{
    let props:ISquareProps ;
    const onClick = jest.fn(()=>null);
    beforeEach(()=>{
        props = {
            squareOnClick: onClick,
            value: "X"
        }
    })

    it("should Propagate changes to squareOnCick",async ()=>{
        render(<Square {...props}/>)
        fireEvent.click(screen.getByText('X'));
        expect(onClick.mock.calls.length).toEqual(1);
    })
});