import { IRootState } from './store';
import { addWinner } from './score/thunks';

import React from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Form, Label, Input } from 'reactstrap';
import {useForm} from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';

interface IWinningModalProps{
    tryAgain: ()=> void
    winner: string
}

interface IWinningModalForm{
    name:string,
    email:string,
    gender:string
}

export default function WinningModal(props:IWinningModalProps){
    const squares = useSelector((state:IRootState)=>state.board.squares);
    const {register,handleSubmit} = useForm<IWinningModalForm>({
        defaultValues:{
            name:"",
            email:"",
            gender:""
        }
    });

    const dispatch = useDispatch();

    const onSubmit = (data:IWinningModalForm)=>{
        dispatch(addWinner(data.name,squares,props.winner))
        props.tryAgain();
    }

    return (
        <Modal isOpen={true}  >
            <ModalHeader>Congratulations!</ModalHeader>
            <ModalBody>
                Congratulations! {props.winner} win the Game !!
                Please input your name and email here!!
                <Form onSubmit={handleSubmit(onSubmit)}>
                    <Label>
                        Name:
                        <Input type='text' name="name" innerRef={register}/>
                    </Label>
                    <Label>
                        Email
                        <Input type='email' name="email" innerRef={register}/>
                    </Label>
                   <Label>
                        Gender
                        <Input type="select" name="gender" innerRef={register}>
                            <option value="">Please Select</option>
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                        </Input>
                    </Label>
                    <Input type='submit' value="Submit" />
                </Form>
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={props.tryAgain}>Save Score and Play Again!</Button>
            </ModalFooter>
        </Modal>
    )
}
