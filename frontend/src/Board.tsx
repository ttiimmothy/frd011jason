import React from 'react';
import Square from './Square';
import './Board.css';
import {IRootState} from './store';
import {useSelector,useDispatch} from 'react-redux';
import WinningModal from './WinningModal';
import {reset,nextStep,getBoard} from './board/thunks';
import {useEffect} from 'react';
import {useParams} from 'react-router-dom';


export default function Board() {

	const squares = useSelector((state: IRootState) => state.board.squares);
	const oIsNext = useSelector((state: IRootState) => state.board.oIsNext);
	const dispatch = useDispatch();
	const {id} = useParams<{id: string;}>();


	useEffect(() => {
		dispatch(getBoard(parseInt(id)));
	},[dispatch,id]);

	const tryAgain = () => {
		dispatch(reset(parseInt(id)));
	};

	const renderSquare = (i: number) =>
		<Square value={squares[i]} squareOnClick={() => handleClick(i)} />;

	const handleClick = (i: number) => {
		const newSquares = squares.slice();
		if(!newSquares[i] && !calculateWinner()) {
			const player = oIsNext ? 'O' : 'X';
			dispatch(nextStep(parseInt(id),player,i));
		}
	};

	const calculateWinner = () => {
		const lines = [
			[0,1,2],
			[3,4,5],
			[6,7,8],
			[0,3,6],
			[1,4,7],
			[2,5,8],
			[0,4,8],
			[2,4,6],
		];
		for(const line of lines) {
			const [a,b,c] = line;
			if(squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
				return squares[a];
			}
		}
		return null;
	};

	const winner = calculateWinner();

	const status: string = winner ?
		`Winner: ${winner}` :
		`Next player: ${oIsNext ? 'O' : 'X'}`;

	return (
		<div>
			<div className="status">{status}</div>
			<div className="board-row">
				{renderSquare(0)}
				{renderSquare(1)}
				{renderSquare(2)}
			</div>
			<div className="board-row">
				{renderSquare(3)}
				{renderSquare(4)}
				{renderSquare(5)}
			</div>
			<div className="board-row">
				{renderSquare(6)}
				{renderSquare(7)}
				{renderSquare(8)}
			</div>
			{winner ?
				<WinningModal
					tryAgain={tryAgain}
					winner={winner} /> :
				""}
		</div>
	);
}
