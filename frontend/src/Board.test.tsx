import '@testing-library/jest-dom';
import React from 'react';
import {render,screen} from '@testing-library/react';
import {useSelector,useDispatch} from 'react-redux';
import {useParams} from 'react-router-dom';
import Board from './Board';
import Square from './Square';

jest.mock('./Square');
jest.mock('react-redux');
jest.mock('react-router-dom',() => ({
	useParams: jest.fn()
}));
jest.mock('connected-react-router',() => ({
	push: jest.fn()
}));

describe('Board Element',() => {
	let dispatch: jest.Mock;
	beforeEach(() => {
		dispatch = jest.fn();
		(Square as any as jest.Mock).mockReturnValue(<div>Square</div>);

		(useSelector as any as jest.Mock).mockReturnValueOnce(['O','X','O',null,null,null,null,null,null])
			.mockReturnValueOnce(true);
		(useParams as any as jest.Mock).mockReturnValue({
			id: "1"
		});

		(useDispatch as any as jest.Mock).mockReturnValue(dispatch);
	});

	it("should contains 9 squares",async () => {
		render(<Board />);
		const elements = screen.getAllByText(/Square/);
		expect(elements.length).toEqual(9);
	});

	it("should contains a status bar",() => {
		render(<Board />);
		const element = screen.getByText(/Next player:/);
		expect(element).toBeDefined();
	});
});