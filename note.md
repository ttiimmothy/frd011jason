# Deployment

## Backend

- [ ] install Docker in local for development and testing

- [ ] Prepare the `Dockerfile` file

```Text
FROM node:lts
WORKDIR /usr/src/app
COPY . .
EXPOSE 8050
CMD yarn install && \
yarn knex migrate:latest && \
yarn knex seed:run && \
node index.js
```

- [ ] Prepare the `docker-compose.yml`

```Text
version: "3" // ubuntu only access the version between 2.2 and 3.3
services:
    tic-tac-toe:
        environment:
            POSTGRES_USER: postgres
            POSTGRES_PASSWORD: postgres
            POSTGRES_DB: tic-tac-toe
            POSTGRES_HOST: postgres
            NODE_ENV: production
            PORT: 5432
        depends_on:
            - postgres
        build:
            context: ./
            dockerfile: ./Dockerfile
        image: "teckyio/tic-tac-toe:latest"
        ports:
             - "8050:8050"
    postgres:
        image: "postgres:13"
        environment:
            POSTGRES_USER: postgres
            POSTGRES_PASSWORD: postgres
            POSTGRES_DB: tic-tac-toe
        ports:
            - "5432:5432"
        volumes:
              - ./pgdata:/var/lib/postgresql/data
```

- [ ] run the command to build and start the container

```Bash
cd server
docker-compose up -d
# docker-compose up // cannot not run in background
```

- [ ] attach shell (e.g postgres)

```Bash
docker exec -it <CONTAINER_ID> bash
```

- [ ] log file checking (e.g `tic-tac-toe`)

```Bash
docker logs --tail 100 -f <CONTAINER_ID> (e.g server_tic-tac-toe_1)
```

- [ ] create account in `Dockerhub`

- [ ] login the `Dockerhub` account

- [ ] create a new repo in `Dockerhub`

- [ ] push the image to `Dockerhub`

	- [ ] login the `Dockerhub` account in terminatl

	```Bash
	docker login
	```

	- [ ] push the image

	```Bash
	docker tag <your-local-image-tag>:<tagname>  <your-account-name>/<your-repo-name>:<tagname>
	docker push <your-account-name>/<your-repo-name>:<tagname>
	```

	(example)

	```Bash
	docker tag teckyio/tic-tac-toe:latest ttiimmothy/ttiimmothhy:latest
	docker push ttiimmothy/ttiimmothhy:latest
	```

- [ ] create an instance in AWS

- [ ] setup config for AWS in local

```Bash
code ~/.ssh/config
```

- [ ] ssh access to the AWS

- [ ] access to the instance

```Bash
ssh ubuntu@<ip>
```

- [ ] rename the host name of instance

```Bash
# change current user to root
sudo -i

# save a new hostname
echo new_hostname > /etc/hostname
```

- [ ] restart the instance on AWS

- [ ] install `Docker` (<https://docs.docker.com/engine/install/ubuntu>)

	- [ ] install using the repository

	- [ ] check whether docker is installed properly in EC2 instance

	```Bash
	docker ps
	```

	should show similar to following result

	```Bash
	Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Get http://%2Fvar%2Frun%2Fdocker.sock/v1.24/containers/json: dial unix /var/run/docker.sock: connect: permission denied
	```

- [ ] add user `ubuntu` to the group docker in order to connect to the Docker daemon

```Bash
sudo usermod -aG docker ubuntu
```

- [ ] logout and login the instance to see the permission can be accessed

```Bash
docker images
```

- [ ] install `Docker Compose` (<https://docs.docker.com/compose/install/>) (following the guildline for linux)

- [ ] docker login

```Bash
docker login
```

- [ ] copy the `docker-compose.yml` file to the server (local)

```Bash
cd server
scp docker-compose.yml ubuntu@<ip>:~
```

- [ ] login to EC2 instance

- [ ] delete the tic-tac-toe build and rename the image to your repository name

```Bash
version: "3"
services:
    tic-tac-toe:
        environment:
            POSTGRES_USER: postgres
            POSTGRES_PASSWORD: postgres
            POSTGRES_DB: tic-tac-toe
            POSTGRES_HOST: postgres
            NODE_ENV: production
            PORT: 5432
        depends_on:
            - postgres
        image: "ttiimmothy/ttiimmothhy:latest"
        ports:
            - "8040:8050"
    postgres:
        image: "postgres:13"
        environment:
            POSTGRES_USER: postgres
            POSTGRES_PASSWORD: postgres
            POSTGRES_DB: tic-tac-toe
        ports:
            - "5432:5432"
        volumes:
			- ./pgdata:/var/lib/postgresql/data
```

- [ ] pull the docker image (server)

```Bash
docker-compose pull
docker-compose up -d
```

- [ ] install related packages

```Bash
sudo apt-get install nginx htop curl
```

&nbsp;

## Frontend

- [ ] setup AWS CLI

```Bash
aws configure
```

- [ ] record domain name of cloudfront in AWS

![Croudfront](./awsCroudfront.png)

&nbsp;

## CI/CD

1. server-testing
2. react-testing
3. server-build
4. react-build
5. server-deploy
6. react-deploy

### server-build

- login Dockerhub
- build docker image
- push docker image from CI Runner to Dockerhub

### server-deploy

- clone docker compose config from CI Runner to EC2
- ssh EC2
- docker login
- docker pull (Get latest image from Dockerhub)
- build and restart containers (similar to forever restart)

### create production branch

```Bash
# For the first time, create the production branch based on master branch
git branch production master

# Stand on the production branch
git checkout production

# Merge the changes from master branch
git merge master

# Push the production to remote (gitlab)
git push origin production

# Stand back on the master branch for future commits
git checkout master
```